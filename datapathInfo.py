class customDpset(object):
    def __init__(self):
        self.dpInfoList = {}

    def addDpInfo(self, dpInfo):
        self.dpInfoList[dpInfo.dpid] = dpInfo

    def getDatapathByID(self, dpID):
        return self.dpInfoList[dpID].dp

    def removeDpInfo(self, dpInfo):
        del self.dpInfoList[dpInfo.dpid]
 
    def getPortByNumber(self, portNumber, dpID):
        for port in self.dpInfoList[dpID].portList:
            if port.port_no == portNumber:
                 return port
        print 'No port with that ID exists in this datapath'

class dpInfo(object):
    def __init__(self, dp, dpid):
        self.dp = dp
        self.dpid = dpid
        self.connections = {}
        self.portList = []
