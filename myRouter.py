from __future__ import absolute_import
from myLogger import Logger
from ryu.base import app_manager
from ryu.controller import ofp_event, mac_to_port
from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER, set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import arp, ethernet, ether_types, icmp, ipv4, packet
from ryu.lib import ofctl_v1_3 as ofctl
import myRoutingMethods as routing
from ryu.lib.packet import ether_types as ETH

ETHERNET_FLOOD = 'ff:ff:ff:ff:ff:ff'
INFO = 'INFO'
WARNING = 'WARNING'
SEVERE = 'SEVERE'

class myRouter(object):

    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, hw_addr, ip_addr, *args, **kwargs):
        #We have to call the super init.
        super(myRouter, self).__init__(*args, **kwargs)
	#Setting up an empty dictionary to hold information obtained from the packet.
        self.mac_to_port = {}
        #NOT A CONVERTER! This is for mapping IP addresses to MAC addresses
        self.ip_to_mac = {}
        self.hw_addr= hw_addr
        self.ip_addr= ip_addr
        self.logger = Logger()

        self.logger.openFile('routerLog', 'w')       

    def __del__(self):
        self.logger.logToFile(INFO, 'Closing log.')
        self.logger.closeFile()

    '''
    def ICMPHandler(self, ethPacket, icmpPacket, ipv4Packet, inPort):
        if icmpPacket.type != icmp.ICMP_ECHO_REQUEST:
            return
        myPacket = packet.Packet()
        myPacket.add_protocol(ethernet.ethernet(ethertype=ethPacket.ethertype, dst=ethPacket.src, src=self.hw_addr))
        myPacket.add_protocol(ipv4.ipv4(dst=ipv4Packet.src, src=self.ip_addr, proto=ipv4Packet.proto)
        myPacket.add_protocol(icmp.icmp(type_=icmp.ICMP_ECHO_REPLY, code=icmp.ICMP_ECHO_REPLY_CODE, csum=0, data=icmpPacket.data))
        return myPacket
    '''
    def setDefaultFlowmods(self, message):
        datapath = message.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inboundArpActions=[parser.OFPActionOutput(port=ofproto.OFPP_CONTROLLER)]
        inboundArpInstructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, inboundArpActions)]
        inboundArpMatch = parser.OFPMatch(eth_type=ETH.ETH_TYPE_ARP, arp_tpa=self.ip_addr)

        self.logger.logToCLI(INFO, 'Creating default flowmod for ARP packets destined for this application')
        inboundArpFlowmod=datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=inboundArpMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=50000, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=inboundArpInstructions) 
        inboundArpFlowmod.xid = 10
        datapath.send_msg(inboundArpFlowmod)

    def switch_features_handler(self, message):
        self.setDefaultFlowmods(message)
        self.logger.logToCLI(INFO, 'Default flowmods set')

    def packet_in_handler(self, message):
        #print dir(message.datapath)
        datapath = message.datapath
        datapathID = datapath.id
        self.logger.logToCLI(INFO, 'Packet received by packet in event on datapath {0}'.format(datapathID))
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inPort = message.match['in_port']
        actions = []

	#Packets with an invalid ttl should not be processed
 #       if message.reason == ofproto.OFPR_INVALID_TTL:
        #Possible future feature; let sender know about invalid ttl reason
 #           self.logger.log(WARNING, 'Packet with invalid TTL dropped')
 #           return
        
        #Getting the information from the packet
        myPacket = packet.Packet(message.data)
        ethPacket = myPacket.get_protocol(ethernet.ethernet)
  	#Checking to see if it's using a valid ethernet protocol. Drop it if not.
        if not ethPacket:
            self.logger.logToCLI(WARNING, 'Packet without ethernet dropped')
            return

	#Try to get the protocol for the 3 types of packets we'll be handling
        arpPacket = myPacket.get_protocol(arp.arp)
        icmpPacket = myPacket.get_protocol(icmp.icmp)
        ipv4Packet = myPacket.get_protocol(ipv4.ipv4)

    	#Check to see if the packet has that protocol
        if arpPacket:
            self.logger.logToCLI(INFO, 'ARP packet received')
            if arpPacket.dst_ip == self.ip_addr:
                if arpPacket.opcode == arp.ARP_REQUEST:
                    outPacket = routing.ARPResponse(arpPacket, ethPacket, self.ip_addr, self.hw_addr, inPort)
                    self.logger.logToCLI(INFO, 'Send ARP response to IP address {0} with MAC address {1}'.format(arpPacket.src_ip, arpPacket.src_mac))
                    actions = [parser.OFPActionOutput(inPort)]                
                    routing.sendPacket(datapath, inPort, outPacket, actions)
                elif arpPacket.opcode == arp.ARP_REPLY:
                    routing.learnIP(datapathID, arpPacket.src_mac, arpPacket.src_ip, self.ip_to_mac)
                    self.logger.logToCLI(INFO, 'ARP reply received from IP address {0} with MAC address {1}'.format(arpPacket.src_ip, arpPacket.src_mac))
            else:
                destination = ethPacket.dst
                source = ethPacket.src
                self.logger.logToCLI(INFO, 'ARP source MAC: {0} ARP source IP: {1} ARP destination MAC: {2} ARP destination IP: {3}'.format(arpPacket.src_mac, arpPacket.src_ip, arpPacket.dst_mac, arpPacket.dst_ip))
                routing.learnMAC(datapathID, inPort, source, self.mac_to_port)
                self.logger.logToCLI(INFO, 'Mapped MAC address {0} to port {1} on datapath {2}'.format(source, inPort, datapathID))
                
                if destination in self.mac_to_port[datapathID]:
                    outPort = self.mac_to_port[datapathID][destination]
                    self.logger.logToCLI(INFO, 'Preparing to send ARP packet to {0} from port {1} on datapath {2}'.format(destination, outPort, datapathID))
                else:
                    outPort = ofproto.OFPP_FLOOD
                    self.logger.logToCLI(INFO, 'Preparing to send ARP packet out flood port.')

                actions = [parser.OFPActionOutput(outPort)]
 
                if outPort != ofproto.OFPP_FLOOD:
                    arpMatch = ofctl.to_match(datapath, {'eth_dst':destination, 'arp_op':arp.ARP_REPLY})
                    self.logger.logToCLI(INFO, 'Creating flowmod for ARP replies destined for {0}'.format(destination))
                    instructions=[parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
                    routing.addFlow(datapath, arpMatch, instructions)

                data = None
                if message.buffer_id == ofproto.OFP_NO_BUFFER:
                    data = message.data
            
                out = datapath.ofproto_parser.OFPPacketOut(datapath=datapath, buffer_id=message.buffer_id, in_port=inPort, actions=actions, data=data)

                datapath.send_msg(out)
                self.logger.logToCLI(INFO, 'Sending out ARP packet.')
            self.logger.logToCLI(INFO, '-------- End of Packet In event --------')
            return
     
        if ipv4Packet:         
            if icmpPacket:
                '''
                self.logger.logToCLI(INFO, 'ICMP packet received')
                outPacket = self.ICMPHandler(ethPacket, icmpPacket, ipv4Packet, inPort)
                actions = []
                self.sendPacket(datapath, inPort, outPacket, actions)
                '''
                pass
            else:  
                self.logger.logToCLI(INFO, 'IPv4 packet received')
                

                datapathID = datapath.id
                routing.learnIP(datapathID, ethPacket.src, ipv4Packet.src, self.ip_to_mac)
                self.logger.logToCLI(INFO, 'Mapped IP address {0} to MAC address {1} on datapath {2}'.format(ipv4Packet.src, ethPacket.src, datapathID))

                dstIP = ipv4Packet.dst
                if dstIP in self.ip_to_mac[datapathID]:
                    ethDst = self.ip_to_mac[datapathID][dstIP]
                    self.logger.logToCLI(INFO, 'Setting next hop MAC address to {0}'.format(ethDst))
                else:
                    routing.sendARPRequest(datapath, dstIP, ethPacket, self.hw_addr, self.ip_addr)
                    self.logger.logToCLI(INFO, 'Sending ARP request for IP address {0} and dropping packet.'.format(dstIP))
                    self.logger.logToCLI(INFO, '-------- End of Packet In event --------')
                    return

                self.logger.logToCLI(INFO, 'IP Source: {0} IP Destination: {1} Ethernet Source: {2} Ethernet Destination: {3}'.format(ipv4Packet.src, ipv4Packet.dst, ethPacket.src, ethDst))
 
                outPacket = routing.IPV4Packet(ethPacket, ethDst, self.hw_addr, ipv4Packet, inPort)

                #We store the port that the packet with this datapath ID from this source (mac address) came from 
		#so we can reuse it in the future, instead of having to use the FLOOD port.
                
                routing.learnMAC(datapathID, inPort, ethPacket.src, self.mac_to_port)
                self.logger.logToCLI(INFO, 'Mapped MAC address {0} to port {1} on datapath {2}'.format(ethPacket.src, inPort, datapathID))

	        #We're seeing if we know where this thing goes. If we do, we send it there, otherwise we send it to FLOOD
		#so we can figure out where it's supposed to go.
                if ethDst in self.mac_to_port[datapathID]:
                    outPort = self.mac_to_port[datapathID][ethDst]
                    self.logger.logToCLI(INFO, 'Preparing to send ipv4 packet out port {0}'.format(outPort))
                else:
                    outPort = ofproto.OFPP_FLOOD
                    self.logger.logToCLI(INFO, 'Preparing to send ipv4 packet out flood port')

                #Setting an action to tell the switch where to send the packet, as determined above.
                actions= [parser.OFPActionDecNwTtl(), parser.OFPActionOutput(outPort)]

	        #The in_port event is actually for the controller, and is only triggered when there's a flow miss,
		#if we have an action specifically send it up to the controller, or if the TLL is no longer valid. 
		#We can't have the controller do this for every single packet that the network processes, 
		#so we need to create a flow so the switches can match to it and send the packet on its way and not bother the controller.
                if outPort != ofproto.OFPP_FLOOD:
                     match = ofctl.to_match(datapath, {'eth_dst':ethDst})
                     instructions=[parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
                     self.logger.logToCLI(INFO, 'Creating flowmod for packets destined for MAC address {0}'.format(ethDst))
                     routing.addFlow(datapath, match, instructions)

                routing.sendPacket(datapath, inPort, outPacket, actions)
        
        self.logger.logToCLI(INFO, '-------- End of Packet In event --------')
