import inspect

class Logger():
    def openFile(self, filename, mode):
        self.file = open(filename, mode)
        
    def logToFile(self, severity, message):
        frame = inspect.stack()[1]
        callingFunc = inspect.getsource(frame[0])
        funcName = callingFunc.splitlines()
        self.file.write('[' + severity + '] at' + funcName[0].replace('def', '', 1) + '\n' + message + '\n')

    def logToCLI(self, severity, message):
        print '[' + severity + ']', message

    def closeFile(self):
        self.file.close()

    def readFile(self):
        self.file.read()
    
