from __future__ import absolute_import
from Tkinter import *
import Tkinter            
import tkMessageBox
from ryu.lib.packet import arp, ethernet, ether_types, icmp, ipv4, packet
from ryu.lib import ofctl_v1_3 as ofctl
from ryu.controller.dpset import DPSet
import myRoutingMethods as routing
import datapathInfo


entryRecordList = []

class entryRecord():
    def __init__(self, inputs):
        self.inputs = inputs

def onAddClick():
    inputsList = grabInputsList()
    inputsDict = grabInputsDict()
    recordChange(inputsList, "ADD")
    firewallDatapath = firewallDPset.getDatapathByID(int(datapathIDEntry.get()))
    parser = firewallDatapath.ofproto_parser
    ofproto = firewallDatapath.ofproto
    match = ofctl.to_match(firewallDatapath, inputsDict)
    #outPort = firewallDPset.getPortByNumber(outPortEntry.get(), firewallDapath.id)
    actions = [parser.OFPActionOutput(int(outPortEntry.get()))]
    instructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
    priority = priorityEntry.get()
    if priority == '':
        priority = ofproto.OFP_DEFAULT_PRIORITY
    else:
        priority = int(priority)

    idleTimeout = idleTimeoutEntry.get()
    if idleTimeout == '':
        idleTimeout = ofproto.OFP_DEFAULT_PRIORITY
    else:
        idleTimeout = int(idleTimeout)

    hardTimeout = hardTimeoutEntry.get()
    if hardTimeout == '':
        hardTimeout = ofproto.OFP_DEFAULT_PRIORITY
    else:
        hardTimeout = int(hardTimeout)
    routing.addFlow(firewallDatapath, match, instructions, priority, idleTimeout, hardTimeout)

def onRemoveClick():
    inputsList = grabInputs()
    inputsDict = grabInputsDict()
    recordChange(inputsList, "REMOVE")
    firewallDatapath = firewallDPset.getDatapathByID(int(datapathIDEntry.get()))
    parser = firewallDatapath.ofproto_parser
    ofproto = firewallDatapath.ofproto
    match = ofctl.to_match(firewallDatapath, inputsDict)
    #outPort = firewallDPset.getPortByNumber(outPortEntry.get(), firewallDapath.id)
    actions = [parser.OFPActionOutput(int(outPortEntry.get()))]
    instructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
    priority = priorityEntry.get()
    if priority == '':
        priority = ofproto.OFP_DEFAULT_PRIORITY
    else:
        priority = int(priority)

    idleTimeout = idleTimeoutEntry.get()
    if idleTimeout == '':
        idleTimeout = 0
    else:
        idleTimeout = int(idleTimeout)

    hardTimeout = hardTimeoutEntry.get()
    if hardTimeout == '':
        hardTimeout = 0
    else:
        hardTimeout = int(hardTimeout)
    routing.addFlow(firewallDatapath, match, instructions, priority, idleTimeout, hardTimeout)

def clearInputs(inputList):
    for i in inputList:
        i.delete(0, END)

def recordChange(inputList, addOrRemove):
    global recordNumber
    entryRecordList.append(entryRecord(inputList))
    history.insert(END, "Entry {0}: {1}".format(recordNumber, addOrRemove))
    recordNumber += 1

def onListEntryClick(notUsed):
    lineNumber = history.curselection()
    inputs = entryRecordList[lineNumber[0]].inputs
    for i in range(len(entryList)):
        entryList[i].delete(0, END)       
    	entryList[i].insert(0, inputs[i])

def grabInputsList():
    inputsList = [inPortEntry.get(),
              outPortEntry.get(),
              datapathIDEntry.get(),
              ethDstEntry.get(),
              ethSrcEntry.get(),
              ethTypeEntry.get(),
              vlanIDEntry.get(),
              vlanPriorityEntry.get(),
              ipProtocolEntry.get(),
              ipv4SrcEntry.get(),
              ipv4DstEntry.get(),
              tcpSrcEntry.get(),
              tcpDstEntry.get(),
              udpSrcEntry.get(),
              udpDstEntry.get(),
              sctpSrcEntry.get(),
              sctpDstEntry.get(),
              icmpv4TypeEntry.get(),
              icmpv4CodeEntry.get(),
              arpOpEntry.get(),
              arpSpaEntry.get(),
              arpTpaEntry.get(),
              arpShaEntry.get(),
              arpThaEntry.get(), 
              priorityEntry.get(),
              idleTimeoutEntry.get(),
              hardTimeoutEntry.get()]
    return inputsList

def grabInputsDict():
    inputsDict = {'in_port': inPortEntry.get(),
               'eth_dst': ethDstEntry.get(),
               'eth_src': ethSrcEntry.get(),
               'eth_type': ethTypeEntry.get(),
               'vlan_vid': vlanIDEntry.get(),
               'vlan_pcp': vlanPriorityEntry.get(),
               'ip_proto': ipProtocolEntry.get(),
               'ipv4_src': ipv4SrcEntry.get(),
               'ipv4_dst': ipv4DstEntry.get(),
               'tcp_src': tcpSrcEntry.get(),
               'tcp_dst': tcpDstEntry.get(),
               'udp_src': udpSrcEntry.get(),
               'udp_dst': udpDstEntry.get(),
               'sctp_src': sctpSrcEntry.get(),
               'sctp_dst': sctpDstEntry.get(),
               'icmpv4_type': icmpv4TypeEntry.get(),
               'icmpv4_code': icmpv4CodeEntry.get(),
               'arp_op': arpOpEntry.get(),
               'arp_spa': arpSpaEntry.get(),
               'arp_tpa': arpTpaEntry.get(),
               'arp_sha': arpShaEntry.get(),
               'arp_tha': arpThaEntry.get()}
    for key, value in inputsDict.items():
        if value == '':
            del inputsDict[key]
    return inputsDict


"""
    eth_dst          MAC address     Ethernet destination address
    eth_src          MAC address     Ethernet source address
    eth_type         Integer 16bit   Ethernet frame type
    vlan_vid         Integer 16bit   VLAN id
    vlan_pcp         Integer 8bit    VLAN priority
    ip_dscp          Integer 8bit    IP DSCP (6 bits in ToS field)
    ip_ecn           Integer 8bit    IP ECN (2 bits in ToS field)
    ip_proto         Integer 8bit    IP protocol
    ipv4_src         IPv4 address    IPv4 source address
    ipv4_dst         IPv4 address    IPv4 destination address
    tcp_src          Integer 16bit   TCP source port
    tcp_dst          Integer 16bit   TCP destination port
    udp_src          Integer 16bit   UDP source port
    udp_dst          Integer 16bit   UDP destination port
    sctp_src         Integer 16bit   SCTP source port
    sctp_dst         Integer 16bit   SCTP destination port
    icmpv4_type      Integer 8bit    ICMP type
    icmpv4_code      Integer 8bit    ICMP code
    arp_op           Integer 16bit   ARP opcode
    arp_spa          IPv4 address    ARP source IPv4 address
    arp_tpa          IPv4 address    ARP target IPv4 address
    arp_sha          MAC address     ARP source hardware address
    arp_tha          MAC address     ARP target hardware address
"""
def startGUI(dpSet):
    gui = Tkinter.Tk()
    gui.title("Firewall GUI")
    global history, firewallDPset
    firewallDPset = dpSet
    print firewallDPset.dpInfoList
    history = Listbox(gui, height=40)
    history.grid(row=0, rowspan=10)
    history.bind('<<ListboxSelect>>', onListEntryClick)
    global recordNumber, inPortEntry, outPortEntry, datapathIDEntry, ethDstEntry, ethSrcEntry, ethTypeEntry, vlanIDEntry, vlanPriorityEntry, ipProtocolEntry, ipv4SrcEntry, ipv4DstEntry, tcpSrcEntry, tcpDstEntry, udpSrcEntry, udpDstEntry, sctpSrcEntry, sctpDstEntry, icmpv4TypeEntry, icmpv4CodeEntry, arpOpEntry, arpSpaEntry, arpTpaEntry, arpShaEntry, arpThaEntry, priorityEntry, idleTimeoutEntry, hardTimeoutEntry
              
    recordNumber = 1

    #In Port, Out Port, and Datapath
    inPortLabel = Label(gui, text="In Port:")
    inPortLabel.grid(row=0, column=1, padx=5)
    inPortEntry = Entry(gui)
    inPortEntry.grid(row=0, column=2, padx=5)
    outPortLabel = Label(gui, text="Out Port:")
    outPortLabel.grid(row=0, column=3, padx=5)
    outPortEntry = Entry(gui)
    outPortEntry.grid(row=0, column=4, padx=5)
    datapathIDLabel = Label(gui, text="Datapath ID:")
    datapathIDLabel.grid(row=0, column=5, padx=5)
    datapathIDEntry = Entry(gui)
    datapathIDEntry.grid(row=0, column=6, padx=5)
    
    #Ethernet
    ethDstLabel = Label(gui, text="Ethernet Destination:")
    ethDstLabel.grid(row=1, column=1, padx=5)
    ethDstEntry = Entry(gui)
    ethDstEntry.grid(row=1, column=2, padx=5)
    ethSrcLabel = Label(gui, text="Ethernet Source:")
    ethSrcLabel.grid(row=1, column=3, padx=5)
    ethSrcEntry = Entry(gui)
    ethSrcEntry.grid(row=1, column=4, padx=5)
    ethTypeLabel = Label(gui, text="Ethernet Type:")
    ethTypeLabel.grid(row=1, column=5, padx=5)
    ethTypeEntry = Entry(gui)
    ethTypeEntry.grid(row=1, column=6, padx=5)
    
    #VLAN
    vlanIDLabel = Label(gui, text="VLAN ID:")
    vlanIDLabel.grid(row=2, column=1, padx=5)
    vlanIDEntry = Entry(gui)
    vlanIDEntry.grid(row=2, column=2, padx=5)
    vlanPriorityLabel = Label(gui, text="VLAN Priority")
    vlanPriorityLabel.grid(row=2, column=3, padx=5)
    vlanPriorityEntry = Entry(gui)
    vlanPriorityEntry.grid(row=2, column=4, padx=5)
    
    #IP
    ipProtocolLabel = Label(gui, text="IP Protocol")
    ipProtocolLabel.grid(row=3, column=1, padx=5)
    ipProtocolEntry = Entry(gui)
    ipProtocolEntry.grid(row=3, column=2, padx=5)
    ipv4SrcLabel = Label(gui, text="IPv4 Source:")
    ipv4SrcLabel.grid(row=3, column=3, padx=5)
    ipv4SrcEntry = Entry(gui)
    ipv4SrcEntry.grid(row=3, column=4, padx=5)
    ipv4DstLabel = Label(gui, text="IPv4 Destination:")
    ipv4DstLabel.grid(row=3, column=5, padx=5)
    ipv4DstEntry = Entry(gui)
    ipv4DstEntry.grid(row=3, column=6, padx=5)
    
    #TCP/UDP
    tcpSrcLabel = Label(gui, text="TCP Source Port:")
    tcpSrcLabel.grid(row=4, column=1, padx=5)
    tcpSrcEntry = Entry(gui)
    tcpSrcEntry.grid(row=4, column=2, padx=5)
    tcpDstLabel = Label(gui, text="TCP Destination Port:")
    tcpDstLabel.grid(row=4, column=3, padx=5)
    tcpDstEntry = Entry(gui)
    tcpDstEntry.grid(row=4, column=4, padx=5)
    udpSrcLabel = Label(gui, text="UDP Source Port:")
    udpSrcLabel.grid(row=4, column=5, padx=5)
    udpSrcEntry = Entry(gui)
    udpSrcEntry.grid(row=4, column=6, padx=5)
    udpDstLabel = Label(gui, text="UDP Destination Port:")
    udpDstLabel.grid(row=4, column=7, padx=5)
    udpDstEntry = Entry(gui)
    udpDstEntry.grid(row=4, column=8, padx=5)
    
    #SCTP
    sctpSrcLabel = Label(gui, text="SCTP Source:")
    sctpSrcLabel.grid(row=5, column=1, padx=5)
    sctpSrcEntry = Entry(gui)
    sctpSrcEntry.grid(row=5, column=2, padx=5)
    sctpDstLabel = Label(gui, text="SCTP Destination:")
    sctpDstLabel.grid(row=5, column=3, padx=5)
    sctpDstEntry = Entry(gui)
    sctpDstEntry.grid(row=5, column=4, padx=5)
    
    #ICMP
    icmpv4TypeLabel = Label(gui, text="ICMP Type:")
    icmpv4TypeLabel.grid(row=6, column=1, padx=5)
    icmpv4TypeEntry = Entry(gui)
    icmpv4TypeEntry.grid(row=6, column=2, padx=5)
    icmpv4CodeLabel = Label(gui, text="ICMP Label:")
    icmpv4CodeLabel.grid(row=6, column=3, padx=5)
    icmpv4CodeEntry = Entry(gui)
    icmpv4CodeEntry.grid(row=6, column=4, padx=5)
    
    #ARP
    arpOpLabel = Label(gui, text="ARP Opcode:")
    arpOpLabel.grid(row=7, column=1, padx=5)
    arpOpEntry = Entry(gui)
    arpOpEntry.grid(row=7, column=2, padx=5)
    arpSpaLabel = Label(gui, text="ARP IP Source:")
    arpSpaLabel.grid(row=8, column=1, padx=5)
    arpSpaEntry = Entry(gui)
    arpSpaEntry.grid(row=8, column=2, padx=5)
    arpTpaLabel = Label(gui, text="ARP IP Destination:")
    arpTpaLabel.grid(row=8, column=3, padx=5)
    arpTpaEntry = Entry(gui)
    arpTpaEntry.grid(row=8, column=4, padx=5)
    arpShaLabel = Label(gui, text="ARP MAC Source:")
    arpShaLabel.grid(row=8, column=5, padx=5)
    arpShaEntry = Entry(gui)
    arpShaEntry.grid(row=8, column=6, padx=5)
    arpThaLabel = Label(gui, text="ARP MAC Destination:")
    arpThaLabel.grid(row=8, column=7, padx=5)
    arpThaEntry = Entry(gui)
    arpThaEntry.grid(row=8, column=8, padx=5)
    
    #Flowmod
    priorityLabel = Label(gui, text="Priority:")
    priorityLabel.grid(row=9, column=1, padx=5)
    priorityEntry = Entry(gui)
    priorityEntry.grid(row=9, column=2, padx=5)
    idleTimeoutLabel = Label(gui, text="Idle Timeout:")
    idleTimeoutLabel.grid(row=9, column=3, padx=5)
    idleTimeoutEntry = Entry(gui)
    idleTimeoutEntry.grid(row=9, column=4, padx=5)
    hardTimeoutLabel = Label(gui, text="Hard Timeout:")
    hardTimeoutLabel.grid(row=9, column=5, padx=5)
    hardTimeoutEntry = Entry(gui)
    hardTimeoutEntry.grid(row=9, column=6, padx=5)
    
    clearInputsButton = Button(gui, text="Clear inputs", command=lambda:clearInputs(entryList))
    clearInputsButton.grid(row=10, column=3, pady=5)
    addFlowButton = Button(gui, text="Add flowmod", command=onAddClick)
    addFlowButton.grid(row=10, column=4, pady=5)
    removeFlowButton = Button(gui, text="Remove flowmod", command=onRemoveClick)
    removeFlowButton.grid(row=10, column=5, pady=5)
    
    global entryList
    entryList= [inPortEntry,
               outPortEntry,
               datapathIDEntry,
               ethDstEntry,
               ethSrcEntry,
               ethTypeEntry,
               vlanIDEntry,
               vlanPriorityEntry,
               ipProtocolEntry,
               ipv4SrcEntry,
               ipv4DstEntry,
               tcpSrcEntry,
               tcpDstEntry,
               udpSrcEntry,
               udpDstEntry,
               sctpSrcEntry,
               sctpDstEntry,
               icmpv4TypeEntry,
               icmpv4CodeEntry,
               arpOpEntry,
               arpSpaEntry,
               arpTpaEntry,
               arpShaEntry,
               arpThaEntry,
               priorityEntry,
               idleTimeoutEntry,
               hardTimeoutEntry]

    gui.mainloop()


