from __future__ import absolute_import
import myRouter, mySwitch, myFirewall, firewallGUI, datapathInfo
import myRoutingMethods as routing
from myLogger import Logger
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import arp, ethernet, ether_types, icmp, ipv4, packet
from ryu.lib import ofctl_v1_3 as ofctl
from ryu.controller import mac_to_port
from multiprocessing import Process
from netaddr import IPNetwork, IPAddress
import threading

INFO = 'INFO'
WARNING = 'WARNING'
SEVERE = 'SEVERE'
ETHERNET_FLOOD = 'ff:ff:ff:ff:ff:ff'

class myController(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(myController, self).__init__(*args, **kwargs)
        self.logger = Logger()
        self.logger.openFile('controllerLog', 'w')
        self.dpInfo = datapathInfo.customDpset()
        #For the 10.X.X.X subnet
        self.router1 = myRouter.myRouter('b8:8a:9b:d1:ec:02', '10.0.0.200')
        self.network1 = IPNetwork('10.0.0.0/24')
        self.firewall = myFirewall.myFirewall()
        self.switch = mySwitch.mySwitch()
        threading.Timer(60, self.getFlowStats).start()
        
    def __del__(self):
        self.logger.logToFile(INFO, 'Closing log.')
        self.logger.closeFile()

    def getFlowStats(self):
        for dpInfo in self.dpInfo.dpInfoList:
            datapath = self.dpInfo.dpInfoList[dpInfo].dp
            parser = datapath.ofproto_parser
            request = parser.OFPFlowStatsRequest(datapath)
            datapath.send_msg(request)
        self.logger.logToCLI(INFO, 'getFlowStats called')
        threading.Timer(60, self.getFlowStats).start()

    def setDefaultFlowmods(self, message):
        datapath = message.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        self.router1.setDefaultFlowmods(message)
        self.firewall.switch_features_handler(message)
        
        floodActions=[parser.OFPActionOutput(port=ofproto.OFPP_FLOOD)]
        floodInstructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, floodActions)]        
        floodMatch = parser.OFPMatch(eth_dst=ETHERNET_FLOOD)
        self.logger.logToCLI(INFO, 'Creating default flowmod for ARP packets destined for any other machine')
        floodFlowmod=datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=floodMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=20000, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=floodInstructions)
        datapath.send_msg(floodFlowmod)

    @set_ev_cls(ofp_event.EventOFPErrorMsg, [HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER])
    def _error_msg_handler(self, ev):
        message = ev.msg

        self.logger.logToCLI(INFO, 'OFPErrorMessage received. Type: {0} Code: {1} Xid: {2}'.format(message.type, message.code, message.xid))

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        message = ev.msg
        datapath = message.datapath
        datapathID = datapath.id
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        dpInfo = datapathInfo.dpInfo(datapath, datapathID)
        self.logger.logToCLI(INFO, 'Datapath with ID {0} has connected.'.format(datapathID))
        dpInfo.portList = datapath.ports.values()
        self.dpInfo.addDpInfo(dpInfo)        
    
        self.setDefaultFlowmods(message)
    
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        message = ev.msg
        datapath = message.datapath
        datapathID = datapath.id
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        
        myPacket = packet.Packet(message.data)
        ethPacket = myPacket.get_protocol(ethernet.ethernet)
        arpPacket = myPacket.get_protocol(arp.arp)
        ipv4Packet = myPacket.get_protocol(ipv4.ipv4)
       
        if ipv4Packet or arpPacket:
            srcAddress = 0
            dstAddress = 0
            if ipv4Packet:
                srcAddress = ipv4Packet.src
                dstAddress = ipv4Packet.dst
            elif arpPacket:
                srcAddress = arpPacket.src_ip
                dstAddress = arpPacket.dst_ip
            if (IPAddress(srcAddress) in self.network1) or dstAddress == self.router1.ip_addr:
                self.router1.packet_in_handler(message)
            #elif (!(subnetChecker.addressInNetwork(srcAddress, self.network
                #TODO: Send to firewall
        elif ethPacket:
            self.switch.packet_in_handler(message)

           
