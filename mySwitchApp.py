from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.mac import haddr_to_bin
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.controller import mac_to_port

class mySwitch(app_manager.RyuApp):
    def __init__(self, *args, **kwargs):
	#We have to call the super init. Still not sure why.
        super(mySwitch, self).__init__(*args, **kwargs)
	#Setting up an empty dictionary to hold information obtained from the packet.
        self.mac_to_port = {}

    def createFlow(self, datapath, inPort, destination, actions):
        ofproto = datapath.ofproto

	#This is how we match to a flow. Right now, all we care about is where this packet came in from and where its destined.
        dataToMatch = datapath.ofproto_parser.OFPMatch(in_port=inPort, dl_dst=haddr_to_bin(destination))

	#This is where we create the actual flow itself. It can take in a large amount of variables, but we are not using them currently.
        moddedMessage = datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=dataToMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=ofproto.OFP_DEFAULT_PRIORITY, flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(moddedMessage)

    #This is a decorator. It takes in the below function + parameters and returns modified function. They're hard to understand.
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):
        message = ev.msg
        datapath = message.datapath
        ofproto = datapath.ofproto

        #Here we are making an OpenFlow packet, and then holding the ethernet data in another variable.
        myPacket = packet.Packet(message.data)
        ethernetInfo = myPacket.get_protocol(ethernet.ethernet)

	#Obtaining source and destination info from the ethernet data
        destination = ethernetInfo.dst
        source = ethernetInfo.src

	#Obatining the ID of the datapath and seeing if we have it. If not, we create an empty dictionary at this ID.
        datapathID = datapath.id
        self.mac_to_port.setdefault(datapathID, {})
        
        #We store the port that the packet with this datapath ID from this source (mac address) came from so we can reuse it in the future, instead of having to use the FLOOD port.
        self.mac_to_port[datapathID][source] = message.in_port

	#We're seeing if we know where this thing goes. If we do, we send it there, otherwise we send it to FLOOD so we can figure out where it's supposed to go.
        if destination in self.mac_to_port[datapathID]:
            outPort = self.mac_to_port[datapathID][destination]
        else:
            outPort = ofproto.OFPP_FLOOD

        #Setting an action to tell the switch where to send the packet, as determined above.
        actions = [datapath.ofproto_parser.OFPActionOutput(outPort)]

	#The in_port event is actually for the controller, and is only triggered when there's a flow miss, if we have an action specifically send it up to the controller, or if the TLL is no longer valid. We can't have the controller do this for every single packet that the network processes, so we need to create a flow so the switches can match to it and send the packet on its way and not bother the controller.
        if outPort != ofproto.OFPP_FLOOD:
            self.createFlow(datapath, message.in_port, destination, actions)

        #Check the buffer of the physical switch. The switch buffers the packet while we deal with packet_in here on the controller. 
        data = None
        if message.buffer_id == ofproto.OFP_NO_BUFFER:
            data = message.data

	#Finally sending the packet out.
        out = datapath.ofproto_parser.OFPPacketOut(
            datapath=datapath, buffer_id=message.buffer_id, in_port=message.in_port,
            actions=actions, data=data)
        datapath.send_msg(out)
