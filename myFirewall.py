from __future__ import absolute_import
from myLogger import Logger
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import arp, ethernet, ether_types, icmp, ipv4, packet
from ryu.lib import ofctl_v1_3 as ofctl
from ryu.controller import mac_to_port
import myRoutingMethods as routing
from multiprocessing import Process
import firewallGUI
from ryu.lib.packet import ether_types as ETH

#Setting up logger levels
INFO = 'INFO'
WARNING = 'WARNING'
SEVERE = 'SEVERE'

class myFirewall(app_manager.RyuApp):

    def __init__(self, *args, **kwargs):
        #We have to call the super init.
        super(myFirewall, self).__init__(*args, **kwargs)
        self.logger = Logger()
        self.ip_addr = '10.0.0.150'
        self.hw_addr = 'FF:EF:9D:52:B9:CC'
        self.logger.openFile('firewallLog', 'w')
        #guiProcess = Process(target=firewallGUI.startGUI)
        print "Before start"
        #guiProcess.start()
        print "After start"
        

    def __del__(self):
        self.logger.logToFile(self.info, 'Closing log.')
        self.logger.closeFile()

    def switch_features_handler(self, message):
        datapath = message.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        self.logger.logToCLI(INFO, "Firewall with IP of {0} and datapath ID {1} has connected.".format(self.ip_addr, datapath.id))

        #Setting up implicit deny. Lowest priority flowmod; if something does not match against
        #any other flowmod, it will be dropped. An empty actions set indicates to drop the packet.
        emptyActions = []
        emptyInstructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, emptyActions)]
        emptyMatch = parser.OFPMatch()

        #Setting up flowmod for any ARP requests destined for the firewall. They will be sent to
        #the controller and generate a packet_in event, at which point a reply will be generated.
        inboundArpActions = [parser.OFPActionOutput(port=ofproto.OFPP_CONTROLLER)]
        inboundArpInstructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, inboundArpActions)]
        inboundArpMatch = ofctl.to_match(datapath, {'eth_type':ETH.ETH_TYPE_ARP, 'arp_op':arp.ARP_REPLY, 'arp_tpa':self.ip_addr})
 
        implicitAllowActions = [parser.OFPActionOutput(port=ofproto.OFPP_FLOOD)]
        implicitAllowInstructions = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, implicitAllowActions)]
        
        #implicitDenyFlowmod = datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=emptyMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=10, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=emptyInstructions)
        #implicitAllowFlowmod = datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=emptyMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=10, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=implicitAllowInstructions)
        inboundArpRequestFlowmod=datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=inboundArpMatch, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=50000, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=inboundArpInstructions)     
       
        #datapath.send_msg(implicitDenyFlowmod)
        #datapath.send_msg(implicitAllowFlowmod)
        datapath.send_msg(inboundArpRequestFlowmod)
        self.logger.logToCLI(INFO, "Default flowmods created.")

    def packet_in_handler(self, ev):
        message = ev.msg
        datapath = message.datapath
        datapathID = datapath.id
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inPort = message.match['in_port']
        actions = []
 
        myPacket = packet.Packet(message.data)
        arpPacket = myPacket.get_protocol(arp.arp)
        
        if arpPacket.opcode == arp.ARP_REQUEST:
            outPacket = routing.ARPResponse(arpPacket, ethPacket, self.ip_addr, self.hw_addr, inPort)
            actions = [parser.OFPActionOutput(inPort)]                
            routing.sendPacket(datapath, inPort, outPacket, actions)
