from ryu.lib.packet import arp, ethernet, ether_types, icmp, ipv4, packet

ETHERNET_FLOOD = 'ff:ff:ff:ff:ff:ff'

def addFlow(datapath, match, instructions, table_id=0, priority=99999999, idleTimeout=0, hardTimeout=0, returnxid=False):
    ofproto = datapath.ofproto
    if priority == 99999999:
        priority = ofproto.OFP_DEFAULT_PRIORITY
    parser = datapath.ofproto_parser
    moddedMessage = parser.OFPFlowMod(datapath=datapath, match=match, cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=idleTimeout, hard_timeout=hardTimeout, priority=priority, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=instructions, table_id=table_id)
    datapath.set_xid(moddedMessage)
    datapath.send_msg(moddedMessage)
    if returnxid:
        return moddedMessage.xid


def removeFlow(datapath, match, instructions, priority=99999999, idleTimeout=0, hardTimeout=0):
    ofproto = datapath.ofproto
    
    parser = datapath.ofproto_parser
    moddedMessage = datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=match, cookie=0, command=ofproto.OFPFC_DELETE, idle_timeout=idleTimeout, hard_timeout=hardTimeout, priority=priority, flags=ofproto.OFPFF_SEND_FLOW_REM, instructions=instructions)
    datapath.send_msg(moddedMessage)

def ARPResponse(arpPacket, ethPacket, ip_addr, hw_addr, inPort):
    myPacket = packet.Packet()
    dstIP = arpPacket.src_ip
    opCode = arp.ARP_REPLY
    srcIP = ip_addr
    dstMAC = arpPacket.src_mac
    srcMAC = hw_addr
    ethSrc = hw_addr
    ethDst = ethPacket.src

    myPacket.add_protocol(ethernet.ethernet(ethertype=ethPacket.ethertype, dst=ethDst, src=ethSrc))
    myPacket.add_protocol(arp.arp(opcode=opCode, src_mac=srcMAC, src_ip=srcIP, dst_mac=srcMAC, dst_ip=srcIP))
    return myPacket

def sendARPRequest(datapath, dstIP, ethPacket, srcMAC, srcIP):
    ofproto = datapath.ofproto
    parser = datapath.ofproto_parser
    outPacket = packet.Packet()
    outPacket.add_protocol(ethernet.ethernet(ethertype=ether_types.ETH_TYPE_ARP, dst=ETHERNET_FLOOD, src=srcMAC))
    outPacket.add_protocol(arp.arp(opcode=arp.ARP_REQUEST, src_mac=srcMAC, src_ip=srcIP, dst_ip=dstIP))
    outPacket.serialize()
    data = outPacket.data
    actions = [parser.OFPActionOutput(ofproto.OFPP_FLOOD)]
    arpRequest = parser.OFPPacketOut(datapath=datapath, buffer_id=ofproto.OFP_NO_BUFFER, in_port=ofproto.OFPP_CONTROLLER, actions=actions, data=data)
    datapath.send_msg(arpRequest)

def IPV4Packet(ethPacket, ethDst, ethSrc, ipv4Packet, inPort):
    myPacket = packet.Packet()
    myPacket.add_protocol(ethernet.ethernet(ethertype=ethPacket.ethertype, dst=ethDst, src=ethSrc))
    myPacket.add_protocol(ipv4.ipv4(dst=ipv4Packet.dst, src=ipv4Packet.src, proto=ipv4Packet.proto))
    return myPacket

def learnMAC(datapathID, inPort, source, macToPorts):
    macToPorts.setdefault(datapathID, {})
    macToPorts[datapathID][source] = inPort

def learnIP(datapathID, ethSource, ipSource, ipToMac):
    ipToMac.setdefault(datapathID, {})
    ipToMac[datapathID][ipSource] = ethSource

def sendPacket(datapath, port, outPacket, actions):
    ofproto = datapath.ofproto
    parser = datapath.ofproto_parser
    outPacket.serialize()
    data = outPacket.data
    outMessage = parser.OFPPacketOut(datapath=datapath, buffer_id=ofproto.OFP_NO_BUFFER, in_port=port, actions=actions, data=data)
    datapath.send_msg(outMessage)
